//
//  ViewController.swift
//  CollectionView
//
//  Created by Adi Nugroho on 1/5/17.
//  Copyright © 2017 Lonely Box. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource {

    @IBOutlet weak var collView: UICollectionView!
    var items = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for _ in 0...50 {
            items.append(0)
        }
        
        
        collView.dataSource = self
        collView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        return cell
    }
}

extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let secondVC = sb.instantiateViewController(withIdentifier: "VCSecond") as! VCSecond
        present(secondVC, animated: true, completion: nil)
    }
}
